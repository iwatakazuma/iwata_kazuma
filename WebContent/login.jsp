<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./style.css" rel="stylesheet" type="text/css">
	<title>ログイン画面</title>
	</head>
	<body>
		<div class="header">
			<h1>Office Forum</h1>
		</div>
		
			<div class="main-contents">	
				<h2>ユーザーログイン</h2>
				<c:if test="${ not empty errorMessages }">
		                <ul>
		                	<c:forEach items="${errorMessages}" var="message">
		                   		<li class="error-message"><c:out value="${message}" />
		                	</c:forEach>
		            	</ul>
		        	<c:remove var="errorMessages" scope="session"/>
		        </c:if>
		            
			
				<form action="Login" method="post">
					ログインID:<input type="text" name="account"><br>
					パスワード:<input type="password" name="password"><br>
					<input type="submit" value="ログイン">
				</form>
		</div>
	</body>
</html>