<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./style.css" rel="stylesheet" type="text/css">
		<title>ホーム画面</title>
		
		<script type="text/javascript">
	
			function checkDeleteMessage(){
				if(window.confirm("投稿を削除してよろしいですか？")){
					return true;	
				}else {
					window.alert("キャンセルしました")
					return false;
				}
			}
			function checkDeleteComment(){
				if(window.confirm("コメントを削除してよろしいですか？")){
					return true;
				}else {
					window.alert("キャンセルしました")
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="header">
			<h1>Office Forum</h1>
		</div>
	    <ul class="menu-list">
			<li class="menu"><a href="NewMessage">新規投稿</a></li>
			<c:if test="${loginUser.branchId == 1}">
				<li class="menu"><a href="UserManegement">ユーザー管理</a></li>
			</c:if>
			<li class="menu"><a href="Logout">ログアウト</a></li>
		</ul>
		<div class="profile">
			@<c:out value="${loginUser.account}" />
			 <c:out value="${loginUser.name}" />
		</div>
		<c:if test="${ not empty errorMessages }">
	        <ul>
	            <c:forEach items="${errorMessages}" var="message">
	               	<li class="error-message"><c:out value="${message}" />
	            </c:forEach>
	        </ul>
	       	<c:remove var="errorMessages" scope="session"/>
	    </c:if>
		<div class="search">
			<form action="./" method="get">
				<div class="category">カテゴリ <input type="text" name="category" value="${category}"></div>
				<div class="date">日時指定 <input type="date" name="frontDate" min="2018-01-01" value=${frontDate}>
				～ <input type="date" name="endDate" max="today" value=${endDate}>
				<input type="submit" value="検索"></div>
			</form><br>
		</div>
					
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message-comments">
					<div class="message">
					件名<div class="message-title"><c:out value="${message.title}" /></div>
					本文<div class="message-text">
							 <c:forEach var="splitMessage" items="${fn:split(message.text,'
							 ')}"><c:out value="${splitMessage}"/><br>
							 </c:forEach>
						</div>
						<div class="message-name-delete">
							<div class="message-name">
							投稿者：<c:out value="${message.name}" />
								<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<div class="delete-message">
								<c:if test="${loginUser.positionId == 2}">
									<form action="DeleteMessage" method="post" onSubmit="return checkDeleteMessage()">
										<input type="hidden" name="messageId" value="${message.id}">
										<input type="hidden" name="commentId" value="${comment.id}">
										<input type="submit" value="投稿削除">
									</form>
								</c:if>
								<c:if test="${loginUser.id != 2 && message.userId == loginUser.id}">
									<form action="DeleteMessage" method="post" onSubmit="return checkDeleteMessage()">
										<input type="hidden" name="messageId" value="${message.id}">
										<input type="hidden" name="commentId" value="${comment.id}">
										<input type="submit" value="投稿削除">
									</form>
								</c:if>
							</div>
						</div>
					</div>
					
<br>↑へのコメントを入力<div class="comment-form">
						<form action="NewComment" method="post">
						<c:if test="${errorComment.messageId != message.id}">
							<textarea name="text" rows="10" cols="50"></textarea>
						</c:if>
						<c:if test="${errorComment.messageId == message.id}">
							<textarea name="text" rows="10" cols="50">${errorComment.text}</textarea><br>
							<c:remove var="errorComment" scope="session"/>
						</c:if>
						<input type="hidden" name="messageId" value="${message.id}"><br>
						<input type="submit" value="投稿">
						</form>
					</div>
					<p class="comment-tag">コメント一覧</p>
					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
								<c:if test="${comment.messageId == message.id}">
									<div class="comment-name">
										<c:out value="${comment.name}" /><br>
									</div>
									<div class="comment-text">
										<c:forEach var="splitComment" items="${fn:split(comment.text,'
										')}"><c:out value="${splitComment}"/><br>
								  		</c:forEach>
										<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br>
										
										<c:if test="${loginUser.positionId == 2}">
											<form action="DeleteComment" method="post" onSubmit="return checkDeleteComment()">
												<input type="hidden" name="commentId" value="${comment.id}">
												<input type="submit" value="コメント削除">
											</form>
										</c:if>
										<c:if test="${loginUser.positionId != 2 && comment.userId == loginUser.id}">			
											<form action="DeleteComment" method="post" onSubmit="return checkDeleteComment()">
												<input type="hidden" name="commentId" value="${comment.id}">
												<input type="submit" value="コメント削除">
											</form>
										</c:if>
									</div>
								</c:if>
							
							</div>				
						</c:forEach>
						<c:if test="${!commentMessageIds.contains(message.id)}">
							<c:out value="まだコメントはありません"/>
						</c:if>
					</div>
				</div>
			</c:forEach>
		</div>
	</body>
</html>