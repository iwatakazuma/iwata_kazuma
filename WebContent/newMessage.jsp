<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./style.css" rel="stylesheet" type="text/css">
	<title>新規投稿</title>
	</head>
	<body>
		<div class="header">
			<h1>Office Forum</h1>
		</div>
		<ul class="menu-list">
				<li class="menu"><a href="./">ホームに戻る</a></li>
				<li class="menu"><a href="Logout">ログアウト</a></li>
		</ul>
		<c:if test="${ not empty loginUser }">
				<div class="profile">
					@<c:out value="${loginUser.account}" />
					<c:out value="${loginUser.name}" />
				</div>
		</c:if>
		<div class="main-contents">
			<h2>新規投稿</h2>
				<c:if test="${ not empty errorMessages }">
		        	<ul>
		             	<c:forEach items="${errorMessages}" var="message">
		                	<li class="error-message"><c:out value="${message}" />
		               	</c:forEach>
		           	</ul>
		        	<c:remove var="errorMessages" scope="session"/>
		        </c:if>
			<form action="NewMessage" method="post">
				件名(30字以内)<br>
				<input type="text" name="title" size="60" value="${message.title}"><br>
				本文(1000文字以内)<br>
				<textarea name="text" rows="14" cols="75">${message.text}</textarea><br>
				カテゴリー(10文字以内)<br>
				<input type="text" name="category" size="20" value="${message.category}"><br><br>
				<input type="submit" value="投稿"><br><br>
			</form>
		</div>
	</body>
</html>