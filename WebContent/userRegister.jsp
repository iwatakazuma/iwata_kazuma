<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./style.css" rel="stylesheet" type="text/css">
	<title>Office Forum</title>
	</head>
	<body>
		<div class="header"><h1>Office Forum</h1></div>
		<ul class="menu-list">
				<li class="menu"><a href="UserManegement">ユーザー一覧</a>
				<li class="menu"><a href="./">ホームに戻る</a></li>
				<li class="menu"><a href="Logout">ログアウト</a></li>
		</ul>
		<c:if test="${ not empty loginUser }">
				<div class="profile">
					@<c:out value="${loginUser.account}" />
					<c:out value="${loginUser.name}" />
				</div>
		</c:if>
		<div class="main-contents">
			<h2>新規アカウント作成</h2>
				<c:if test="${ not empty errorMessages }">
	                
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li class="error-message"><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>
			<form action="UserRegister" method="post">
				ログインID:<input type="text" name="account" value="${user.account}">(6～20字の半角英数)<br>
				パスワード:<input type="password" name="password">(6～20字の半角英数記号)<br>
				パスワード(確認用):<input type="password" name="confirmPassword"><br>
				氏名:<input type="text" name="name" value="${user.name}"><br>
				支店:<select name="branchId">
						<option value="0"></option>
					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == user.branchId}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${branch.id != user.branchId}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
					</c:forEach>
					</select><br>
				部署・役職:<select name="positionId">
						<option value="0"></option>
					<c:forEach items="${positions}" var="position">
						<c:if test="${position.id == user.positionId}">
							<option value="${position.id}" selected>${position.name}</option>
						</c:if>
						<c:if test="${position.id != user.positionId}">
							<option value="${position.id}">${position.name}</option>
						</c:if>
					</c:forEach>
					</select><br>
				<input type="submit" value="登録">
			</form>
		</div>
	</body>
</html>