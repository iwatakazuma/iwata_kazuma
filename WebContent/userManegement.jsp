<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./style.css" rel="stylesheet" type="text/css">
		<title>ユーザー管理画面</title>
		
		<script type="text/javascript">
		
			function checkStopUser(){
				if(window.confirm("停止してよろしいですか？")){
					return true;	
				}else {
					window.alert("キャンセルされました");
					return false;
				}
			}
			function checkRecoveryUser(){
				if(window.confirm("復活してよろしいですか？")){
					return true;
				}else {
					window.alert("キャンセルされました");
					return false;
				}
			}
		
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1>Office Forum</h1>
		</div>
		<ul class="menu-list">
			<li class="menu"><a href="UserRegister">ユーザー新規登録</a></li>
			<li class="menu"><a href="./">ホームに戻る</a></li>
			<li class="menu"><a href="Logout">ログアウト</a></li>
		</ul>
		<div class="profile">
					@<c:out value="${loginUser.account}" />
					 <c:out value="${loginUser.name}" />
		</div>
		<div class="main-contents">
			<h2>ユーザー情報一覧</h2>
			<c:if test="${ not empty errorMessages }">
		        <ul>
		            <c:forEach items="${errorMessages}" var="message">
		        		<li class="error-message"><c:out value="${message}" />
		            </c:forEach>
		        </ul>
		        <c:remove var="errorMessages" scope="session" />
		    </c:if>
		            
			<table border="1">
				<tr>
					<th>ユーザーID</th>
					<th>氏名</th>
					<th>支店</th>
					<th>部署・役職</th>
					<th>状態</th>
					<th></th>
				</tr>
				<c:forEach items="${users}" var="user">
					<tr>
						<td>${user.account}</td>
						<td>${user.name}</td>
						<c:forEach items="${branches}" var="branch">
							<c:if test="${user.branchId == branch.id}">
								<td>${branch.name}</td>
							</c:if>
						</c:forEach>
						<c:forEach items="${positions}" var="position">
							<c:if test="${user.positionId == position.id}">
								<td>${position.name}</td>
							</c:if>
						</c:forEach>
							<c:if test="${loginUser.id == user.id}">
								<td>
								</td>
							</c:if>
							<c:if test="${loginUser.id != user.id }">
								<c:if test="${user.isDeleted == 0}">
									<td>
										<form action="UserManegement" method="post" onSubmit="return checkStopUser()">
											<input type="hidden" name="id" value="${user.id}">
											<input type="hidden" name="isDeleted" value="${user.isDeleted}">
											<input class="stop" type="submit" value="停止">
										</form>
									</td>
								</c:if>
								<c:if test="${user.isDeleted == 1}">
									<td>
										<form action="UserManegement" method="post" onSubmit="return checkRecoveryUser()">
											<input type="hidden" name="id" value="${user.id}">
											<input type="hidden" name="isDeleted" value="${user.isDeleted}">
											<input class="recovery" type="submit" value="復活">
										</form>
									</td>
								</c:if>
							</c:if>
						<td>
							<form action="UserEdit" method="get" >
								<input type="hidden" name="id" value="${user.id}">
								<input type="submit" value="編集">
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</body>
</html>