package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.User;




@WebFilter("/*")
public class ManegerFilter implements Filter{
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;

		HttpSession session = req.getSession();
		String servletPath = req.getServletPath();
		User user = (User) session.getAttribute("loginUser");
		
		if(servletPath.equals("/UserManegement") || servletPath.equals("/UserRegister")
				|| servletPath.equals("/UserEdit")){
			if(user.getPositionId() == 1){
				chain.doFilter(request, response);
				return;
			}else {
				List<String> messages = new ArrayList<String>();
				messages.add("総務人事担当者以外はアクセスできません");
		        session.setAttribute("errorMessages", messages);
		        res.sendRedirect("./");
				return;
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {


	}

	@Override
	public void destroy() {


	}

}
