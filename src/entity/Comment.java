package entity;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{
	
	private static final long serialVersionUID = 1L;   
	
	//commentsテーブルが持つデータ
	
	private int id;
	private int messageId;
	private String text;
	private int userId;
	private Date created_date;
	private Date updated_date;
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMessageId() {
		return this.messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getUserId() {
		return this.userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreated_date() {
		return this.created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return this.updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

}
