package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;

@WebServlet(urlPatterns = {"/DeleteComment"})
public class DeleteCommentServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
			
		String cId = request.getParameter("commentId");
		int commentId = Integer.parseInt(cId);
		new CommentService().delete(commentId);
        
		response.sendRedirect("./");
			
	}
	
}

