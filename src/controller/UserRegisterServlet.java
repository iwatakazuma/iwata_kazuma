package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import entity.Branch;
import entity.Position;
import entity.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = {"/UserRegister"})
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		
		request.getRequestDispatcher("/userRegister.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String bId = request.getParameter("branchId");
		String pId = request.getParameter("positionId");
		int branchId = Integer.parseInt(bId);
		int positionId = Integer.parseInt(pId);
		
	    User user = new User();
		user.setAccount(account);
		user.setPassword(password);
		user.setName(name);
		user.setBranchId(branchId);
		user.setPositionId(positionId);
		
		if(isValid(request, messages) == true) {
			
			new UserService().register(user);
			response.sendRedirect("UserManegement");
			
		}else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("/userRegister.jsp").forward(request, response);
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");
		
		
		if(StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}else if(!(account.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは6～20字の半角英数字で入力してください");
		}
		if(new UserService().checkRegisterAccount(account) == true){
			messages.add("このログインIDは既に使われています");
		}
		if(StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!(password.matches("[ -~]{6,20}"))) {
			messages.add("パスワードは6～20字の半角英数記号で入力してください");
		}
		if(!(password.equals(confirmPassword))){
			messages.add("パスワードが確認用と一致しません");
		}
		
		if(StringUtils.isBlank(name) == true) {
			messages.add("氏名を入力してください");
		}
		if(name.length() > 10){
			messages.add("氏名は10文字以内で入力してください");
		}
		if(branchId.equals("0")) {
			messages.add("支店名を入力してください");
		}
		if(positionId.equals("0")) {
			messages.add("部署・役職名を入力してください");
		}
		
		if(((branchId.equals("1")) && (positionId.equals("3") || positionId.equals("4"))) || 
				((branchId.equals("2") || branchId.equals("3") || branchId.equals("4")) && (positionId.equals("1") || positionId.equals("2")))){
			messages.add("支店名と部署・役職名の組み合わせが不正です");
		}
		
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
