package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import entity.Message;
import entity.User;
import service.MessageService;


@WebServlet(urlPatterns = {"/NewMessage"})
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("newMessage.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		

		
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");
		
		Message message = new Message();
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");
	
		message.setTitle(title);
		message.setText(text);
		message.setCategory(category);
		message.setUserId(user.getId());
		
		if(isValid(request, messages) == true){

			new MessageService().register(message);
			
			response.sendRedirect("./");
		
		}else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
		}
	}
		
	private boolean isValid(HttpServletRequest request, List<String> messages) {
			
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");
			
		if(StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}
		if(title.length() > 30){
			messages.add("件名は30字以下で入力してください");
		}
		if(StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if(text.length() > 1000){
			messages.add("本文は1000文字以下で入力してください");
		}
		if(StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if(category.length() > 10){
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
}
