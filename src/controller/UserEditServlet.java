package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import entity.Branch;
import entity.Position;
import entity.User;
import service.BranchService;
import service.PositionService;
import service.UserService;


@WebServlet("/UserEdit")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		String i = request.getParameter("id");
		
		if(StringUtils.isBlank(i) == true) {
			messages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("UserManegement");
		}else {
			
			if(new UserService().checkExistId(i) == false){
				messages.add("不正なパラメーターが入力されました");messages.add("不正なパラメーターが入力されました");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("UserManegement");
			}else {
			int id = Integer.parseInt(i);
			List<User> user = new UserService().getUser(id);
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
	
			request.setAttribute("user", user.get(0));
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("/userEdit.jsp").forward(request, response);
			}
		}
	}
		

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));
			
		User user = new User();
		user.setId(id);
		user.setAccount(account);
		user.setPassword(password);
		user.setName(name);
		user.setBranchId(branchId);
		user.setPositionId(positionId);
		
		if(isValid(request, messages, user) == true){
			System.out.println(user.getPassword());
			new UserService().edit(user);
			response.sendRedirect("UserManegement");
			
		}else{
			session.setAttribute("errorMessages", messages);
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("/userEdit.jsp").forward(request, response);
		}
	}
	

	
	private boolean isValid(HttpServletRequest request, List<String> messages, User user){
		
		int id = Integer.parseInt(request.getParameter("id"));
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");
		
		if(StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}else if(!(account.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは6～20字の半角英数字で入力してください");
		}
		if(new UserService().checkEditAccount(id, account) == true){
			messages.add("このログインIDは既に使われています");
		}
		if(!(StringUtils.isBlank(password))){
			if(!(password.equals(confirmPassword))){
				messages.add("パスワードが確認用と一致しません");
			}
			if(!(password.matches("[ -~]{6,20}"))){
				messages.add("パスワードは6～20字の半角英数記号で入力してください");
			}
		}
		if(StringUtils.isBlank(password) && StringUtils.isBlank(confirmPassword) == false){
			messages.add("パスワードが確認用と一致しません");
		}
		if(StringUtils.isBlank(name) == true) {
			messages.add("氏名を入力してください");
		}
		if(name.length() > 10){
			messages.add("氏名は10文字以内で入力してください");
		}
		if(StringUtils.isBlank(request.getParameter("branchId"))) {
			messages.add("支店名を入力してください");
		}
		if(StringUtils.isBlank(request.getParameter("positionId"))) {
			messages.add("部署・役職名を入力してください");
		}
		
		if(((branchId.equals("1")) && (positionId.equals("3") || positionId.equals("4"))) || 
				((branchId.equals("2") || branchId.equals("3") || branchId.equals("4")) && (positionId.equals("1") || positionId.equals("2")))){
			messages.add("支店名と部署・役職名の組み合わせが不正です");
		}
		
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
