package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.CommentMessage;
import service.MessageService;

@WebServlet(urlPatterns = {"/DeleteMessage"})
public class DeleteMessageServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
			
		String mId = request.getParameter("messageId");
		int messageId = Integer.parseInt(mId);
	
	
		CommentMessage commentMessage = new CommentMessage();
		commentMessage.setMessageId(messageId);
		new MessageService().delete(commentMessage);
		
		response.sendRedirect("./");
			
	}
	
}
