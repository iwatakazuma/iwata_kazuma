package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import entity.Comment;
import entity.User;
import service.CommentService;

@WebServlet("/NewComment")
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		
		List<String> messages = new ArrayList<String>();
		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("messageId"));
		Comment comment = new Comment();
		comment.setText(text);
		comment.setUserId(user.getId());
		comment.setMessageId(messageId);
		
		if(isValid(request, messages) == true){
			
			new CommentService().register(comment);
			
			response.sendRedirect("./");
		}else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("errorComment", comment);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages){
		String text = request.getParameter("text");
			
		if(StringUtils.isBlank(text) == true) {
			messages.add("コメントは空のまま投稿できません");
		}
		if(text.length() > 500){
			messages.add("コメントは500字以内でしか投稿できません");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
