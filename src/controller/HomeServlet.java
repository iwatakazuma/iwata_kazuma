package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.User;
import entity.UserComment;
import entity.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }
        
        boolean isShowCommentForm;
        if (user != null) {
            isShowCommentForm = true;
        } else {
            isShowCommentForm = false;
        }
       
        String category = request.getParameter("category");
        String frontDate = request.getParameter("frontDate");
        String endDate = request.getParameter("endDate");
        
        List<UserMessage> messages = new MessageService().getMessage(category, frontDate, endDate);
        List<UserComment> comments = new CommentService().getComment(category);
        request.setAttribute("category", category);
        
        request.setAttribute("frontDate", frontDate);
        request.setAttribute("endDate", endDate);
        
        request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		
		request.setAttribute("comments", comments);
		request.setAttribute("isShowCommentForm", isShowCommentForm);
		
		List<Integer> commentMessageIds = new ArrayList<Integer>();
		for(UserComment comment : comments){
			commentMessageIds.add(comment.getMessageId());
		}
		
		request.setAttribute("commentMessageIds", commentMessageIds);
		
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
