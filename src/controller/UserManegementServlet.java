package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Branch;
import entity.Position;
import entity.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = {"/UserManegement"})
public class UserManegementServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		List<User> users = new UserService().getUser();
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
        request.setAttribute("users", users);
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        
		request.getRequestDispatcher("/userManegement.jsp").forward(request, response);
		
	}
	
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	String i =request.getParameter("id");
	String isD = request.getParameter("isDeleted");
		int id = Integer.parseInt(i);
		int isDeleted = Integer.parseInt(isD);
		if(isDeleted == 0){
			isDeleted = 1;
			
		}else if(isDeleted == 1){
			isDeleted = 0;
		}else {
			System.out.println("isDeletedの値が不正です");
		}
		
		User user = new User();
		user.setId(id);
		user.setIsDeleted(isDeleted);
		
		new UserService().changeStatus(user);

		
		response.sendRedirect("./UserManegement");
		
	}
}
