package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class DeleteCommentDao {

	public void insert(Connection connection, int commentId){
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			
			sql.append("DELETE FROM comments WHERE id = ?");
				
			ps = connection.prepareStatement(sql.toString());
			
			ps.setInt(1, commentId);
			
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	
	}
	
}
