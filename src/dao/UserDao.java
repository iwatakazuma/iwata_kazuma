package dao;


import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import entity.User;
import exception.SQLRuntimeException;

public class UserDao {
	
	public boolean insert(Connection connection, User user) {
		
		if(isValid(connection, user) == true){
		
			PreparedStatement ps= null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("INSERT INTO users ( ");
				sql.append("account");
	            sql.append(", password");
	            sql.append(", name");
	            sql.append(", branch_id");
	            sql.append(", position_id");
	            sql.append(", is_deleted");            
	            sql.append(", created_date");
	            sql.append(", updated_date");
	            sql.append(") VALUES (");
	            sql.append("?"); // account?
	            sql.append(", ?"); // password
	            sql.append(", ?"); // name
	            sql.append(", ?"); // branch_id
	            sql.append(", ?"); // position_id
	            sql.append(", 0"); //is_deleted
	            sql.append(", CURRENT_TIMESTAMP"); // created_date
	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
	            sql.append(");");
	                        
	       
	            ps = connection.prepareStatement(sql.toString());
	            
	            ps.setString(1, user.getAccount());
	            ps.setString(2, user.getPassword());
	            ps.setString(3, user.getName());
	            ps.setInt(4, user.getBranchId());
	            ps.setInt(5, user.getPositionId());
	            
	            
	            ps.executeUpdate();
	          
	            return true;
	            
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}else {
			return false;
		}

	}
	
	private boolean isValid(Connection connection, User user){
		
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?;";
		
			ps = connection.prepareStatement(sql);
			ps.setString(1, user.getAccount());
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return true;
			}else {
				return false;
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
	public User getUser(Connection connection, String account, String password){
		
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?;";
		
			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			} else if(2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else{
				return userList.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
	
	
	public List<User> getUsers(Connection connection) {
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users;");
			
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
		}
	}
	
public List<User> getUsers(Connection connection, int id) {
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE id = ?;");
			
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
		}
	}	
	
	private List<User> toUserList(ResultSet rs) throws SQLException {
		
		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");

	            
	            User user = new User();
	            user.setId(id);
	            user.setAccount(account);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranchId(branchId);
	            user.setPositionId(positionId);
	            user.setIsDeleted(isDeleted);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);
	            
	            ret.add(user);
			}
			return ret;
		}finally {
			close(rs);
		}
	}
	
	
	public void update(Connection connection, User user) {
		
		PreparedStatement ps= null;
		try {
				if(StringUtils.isBlank(user.getPassword())){
			
					StringBuilder sql = new StringBuilder();
					sql.append("UPDATE users SET ");
					sql.append("account = ?");
				    sql.append(", name = ?");
		            sql.append(", branch_id = ?");
		            sql.append(", position_id = ?");
		            sql.append(", created_date = CURRENT_TIMESTAMP");
		            sql.append(", updated_date = CURRENT_TIMESTAMP");
		            sql.append(" WHERE ");
		            sql.append("id = ?;");
		                        
		            ps = connection.prepareStatement(sql.toString());
		            
		            ps.setString(1, user.getAccount());
		            ps.setString(2, user.getName());
		            ps.setInt(3, user.getBranchId());
		            ps.setInt(4, user.getPositionId());
		            ps.setInt(5, user.getId());
		            ps.executeUpdate();
		            
				}else {
					StringBuilder sql = new StringBuilder();
					sql.append("UPDATE users SET ");
					sql.append("account = ?");
					sql.append(", password = ?");
				    sql.append(", name = ?");
		            sql.append(", branch_id = ?");
		            sql.append(", position_id = ?");
		            sql.append(", created_date = CURRENT_TIMESTAMP");
		            sql.append(", updated_date = CURRENT_TIMESTAMP");
		            sql.append(" WHERE ");
		            sql.append("id = ?;");
		                        
		            ps = connection.prepareStatement(sql.toString());
		            
		            ps.setString(1, user.getAccount());
		            ps.setString(2, user.getPassword());
		            ps.setString(3, user.getName());
		            ps.setInt(4, user.getBranchId());
		            ps.setInt(5, user.getPositionId());
		            ps.setInt(6, user.getId());
		            ps.executeUpdate();
				}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}        
	}
	
	public boolean getAnotherAccount(Connection connection, int id, String account){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT account FROM users WHERE NOT id = ?;";
		
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			List<String> accounts = new ArrayList<String>();
			while(rs.next()){
				accounts.add(rs.getString("account"));
			}
			if(accounts.contains(account)){
				return true;
			}else{
				return false;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public boolean getAccount(Connection connection, String account){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT account FROM users";
		
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<String> accounts = new ArrayList<String>();
			while(rs.next()){
				accounts.add(rs.getString("account"));
			}
			if(accounts.contains(account)){
				return true;
			}else{
				return false;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public boolean existedId(Connection connection, String i){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users";
		
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<String> ids = new ArrayList<String>();
			while(rs.next()){
				ids.add(rs.getString("id"));
			}
			if(ids.contains(i)){
				return true;
			}else{
				return false;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
		
		
	public void changeIsDeleted(Connection connection, User user) {
		
		PreparedStatement ps= null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_deleted = ?");
            sql.append(", created_date = CURRENT_TIMESTAMP");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE ");
            sql.append("id = ?;");
                        
            ps = connection.prepareStatement(sql.toString());
            
            ps.setInt(1, user.getIsDeleted());
            ps.setInt(2, user.getId());
            ps.executeUpdate();
            
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}        
	}
}
