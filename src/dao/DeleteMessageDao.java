package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import entity.CommentMessage;
import exception.SQLRuntimeException;

public class DeleteMessageDao {
	
	public void insert(Connection connection, CommentMessage commentMessage){
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			
			sql.append("DELETE FROM messages WHERE id = ?; ");
				
			ps = connection.prepareStatement(sql.toString());
			
			ps.setInt(1, commentMessage.getMessageId());
			
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	
	}

}
