package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dao.DeleteMessageDao;
import dao.MessageDao;
import dao.UserMessageDao;
import entity.CommentMessage;
import entity.Message;
import entity.UserMessage;

public class MessageService {
			
	public void register(Message message) {
			
		Connection connection = null;
		try {
			connection = getConnection();
					
			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);
					
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
		
	
	
	public List<UserMessage> getMessage(String category,String frontDate,String endDate) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserMessageDao messageDao = new UserMessageDao();
			
			Date d = new Date();
			String currentTime = new SimpleDateFormat("yyyy-MM-dd").format(d);
			String frontDateSQL;
			String endDateSQL;
			if(frontDate == null || frontDate == ""){
				frontDateSQL = "2018-01-01 00:00:00";
			}else {
				frontDateSQL = frontDate + " 00:00:00";
			}
			if(endDate == null || endDate == ""){
				endDateSQL = currentTime + " 23:59:59";
			}else {
				endDateSQL = endDate + " 23:59:59";
			}
			List<UserMessage> ret = messageDao.getUserMessages(connection, category, frontDateSQL, endDateSQL);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public void delete(CommentMessage commentMessage) {
		
		Connection connection = null;
		try {
			connection = getConnection();
					
			DeleteMessageDao deleteMessageDao = new DeleteMessageDao();
			deleteMessageDao.insert(connection, commentMessage);
					
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}

	
