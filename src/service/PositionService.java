package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import dao.PositionDao;
import entity.Position;

public class PositionService {
	
	public List<Position> getPosition() {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			PositionDao positionDao = new PositionDao();
			List<Position> ret = positionDao.getPositions(connection);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
