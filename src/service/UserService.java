package service;


import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import dao.UserDao;
import entity.User;
import util.CipherUtil;

public class UserService {

    public boolean register(User user) {
    	
    	Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
      
            if(userDao.insert(connection, user) == true){
            	commit(connection);
            	return true;
            }else {
            	commit(connection);
            	return false;
            }
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
	
	public List<User> getUser() {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserDao userDao = new UserDao();
			List<User> ret = userDao.getUsers(connection);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
public List<User> getUser(int id) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserDao userDao = new UserDao();
			List<User> ret = userDao.getUsers(connection, id);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void edit(User user) {
		Connection connection = null;
        try {
            connection = getConnection();

            if(!(StringUtils.isEmpty(user.getPassword()))){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);
            
            commit(connection);
        
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		
	}
	public boolean checkEditAccount(int id, String account){
		
		Connection connection = null;
		
		try {
            connection = getConnection();
		connection = getConnection();
		
		UserDao userDao = new UserDao();
		
        if(userDao.getAnotherAccount(connection, id, account) == true){
        	commit(connection);
        	return true;
        }else {
        	commit(connection);
        	return false;
        }
         
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	
	public boolean checkRegisterAccount(String account){
		
		Connection connection = null;
		
		try {
            connection = getConnection();
		connection = getConnection();
		
		UserDao userDao = new UserDao();
		
        if(userDao.getAccount(connection, account) == true){
        	commit(connection);
        	return true;
        }else {
        	commit(connection);
        	return false;
        }
         
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	
public boolean checkExistId(String i){
		
		Connection connection = null;
		
		try {
            connection = getConnection();
		connection = getConnection();
		
		UserDao userDao = new UserDao();
		
        if(userDao.existedId(connection, i) == true){
        	commit(connection);
        	return true;
        }else {
        	commit(connection);
        	return false;
        }
         
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	
	public void changeStatus(User user) {
		
		Connection connection = null;
		try {
			connection = getConnection();
					
			UserDao userDao = new UserDao();
			userDao.changeIsDeleted(connection, user);
					
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	
}