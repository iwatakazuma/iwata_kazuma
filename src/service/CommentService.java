package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import dao.CommentDao;
import dao.DeleteCommentDao;
import dao.UserCommentDao;
import entity.Comment;
import entity.UserComment;

public class CommentService {
	
	public void register(Comment comment) {
		
		Connection connection = null;
		try{
			connection =getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);
			
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	
	public List<UserComment> getComment(String category) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComments(connection);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public void delete(int commentId) {
		
		Connection connection = null;
		try {
			connection = getConnection();
					
			DeleteCommentDao deleteCommentDao = new DeleteCommentDao();
			deleteCommentDao.insert(connection, commentId);
					
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
}
