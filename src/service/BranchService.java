package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import dao.BranchDao;
import entity.Branch;

public class BranchService {
	
	public List<Branch> getBranch() {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranches(connection);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
